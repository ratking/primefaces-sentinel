/*
 * Copyright 2009-2015 PrimeTek.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.primefaces.component.menubar;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.FacesException;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.render.FacesRenderer;

import org.primefaces.component.api.AjaxSource;
import org.primefaces.component.api.UIOutcomeTarget;
import org.primefaces.component.menu.AbstractMenu;
import org.primefaces.component.menu.BaseMenuRenderer;
import org.primefaces.model.menu.MenuElement;
import org.primefaces.model.menu.MenuItem;
import org.primefaces.model.menu.Separator;
import org.primefaces.model.menu.Submenu;
import org.primefaces.util.ComponentTraversalUtils;

@FacesRenderer(componentFamily = "org.primefaces.component",
        rendererType = "org.primefaces.component.SentinelMenubarRenderer")
public class SentinelMenubarRenderer extends BaseMenuRenderer {

    @Override
    protected void encodeScript(FacesContext context, AbstractMenu abstractMenu) throws IOException {
        boolean stateful = true;
        if (stateful) {
            ResponseWriter writer = context.getResponseWriter();
            writer.startElement("script", null);
            writer.writeAttribute("type", "text/javascript", null);
            writer.writeText("Sentinel.restoreMenuState();", null);
            writer.endElement("script");
        }
    }

    @Override
    protected void encodeMarkup(FacesContext context, AbstractMenu abstractMenu) throws IOException {
        Menubar menubar = (Menubar) abstractMenu;
//        String style = menubar.getStyle();
//        String styleClass = menubar.getStyleClass();
//        final String CONTAINER_CLASS = "Unselectable fontRegular";
//        styleClass = styleClass == null ? CONTAINER_CLASS : CONTAINER_CLASS + " " + styleClass;
//
//        ResponseWriter writer = context.getResponseWriter();
//        writer.startElement("div", menubar);
//        writer.writeAttribute("id", "layout-menubar", "id"); // 使用硬编码的固定ID
//        writer.writeAttribute("class", styleClass, "styleClass");
//        if (style != null) {
//            writer.writeAttribute("style", style, "style");
//        }
//        writer.startElement("div", null);
//        writer.writeAttribute("id", "buttonArea", null); // @todo 考虑以后改进
//        writer.writeAttribute("class", "layout-menubarinner-box", null);
//        writer.startElement("a", null);
//        writer.writeAttribute("href", "#", null);
//        writer.writeAttribute("id", "layout-menubar-resize", null);
//        writer.writeAttribute("class", "BordRad3", null);
//        writer.writeAttribute("title", "Menu Resize", null);
//        writer.startElement("i", null);
//        writer.writeAttribute("class", "icon-th-list-2", null);
//        writer.endElement("i");
//        writer.endElement("a");
//        writer.startElement("a", null);
//        writer.writeAttribute("href", "#", null);
//        writer.writeAttribute("id", "layout-menubar-resize2", null);
//        writer.writeAttribute("class", "BordRad3", null);
//        writer.writeAttribute("title", "Open Menu", null);
//        writer.startElement("i", null);
//        writer.writeAttribute("class", "icon-menu", null);
//        writer.endElement("i");
//        writer.endElement("a");
//        writer.endElement("div");

        encodeMenu(context, menubar);
//
//        writer.endElement("div");
    }

    protected void encodeMenu(FacesContext context, AbstractMenu menu) throws IOException {
        ResponseWriter writer = context.getResponseWriter();

//        UIComponent optionsFacet = menu.getFacet("options");
//        if (optionsFacet != null) {
//            optionsFacet.encodeAll(context);
//        }

        writer.startElement("ul", menu);
        writer.writeAttribute("id", menu.getClientId(), null);
        writer.writeAttribute("class", "layout-menubar-container", null);

        //encodeKeyboardTarget(context, menu);
        if (menu.getElementsCount() > 0) {
            int marginLevel = 0;
            encodeElements(context, menu, menu.getElements(), marginLevel);
        }

        writer.endElement("ul");
    }

    protected void encodeElements(FacesContext context, AbstractMenu menu, List<MenuElement> elements, int marginLevel) throws IOException {
        ResponseWriter writer = context.getResponseWriter();

        for (MenuElement element : elements) {
            if (element.isRendered()) {
                if (element instanceof MenuItem) {
                    MenuItem menuItem = (MenuItem) element;
                    String containerStyle = menuItem.getContainerStyle();
                    String containerStyleClass = menuItem.getContainerStyleClass();

                    writer.startElement("li", null);
                    writer.writeAttribute("id", menuItem.getClientId(), null);
                    writer.writeAttribute("role", "menuitem", null);
                    if (containerStyleClass != null) {
                        writer.writeAttribute("class", containerStyleClass, null);
                    }
                    if (containerStyle != null) {
                        writer.writeAttribute("style", containerStyle, null);
                    }
                    encodeMenuItem(context, menu, menuItem, marginLevel);
                    writer.endElement("li");
                } else if (element instanceof Submenu) {
                    Submenu submenu = (Submenu) element;
                    String style = submenu.getStyle();
                    String styleClass = submenu.getStyleClass();

                    writer.startElement("li", null);
                    writer.writeAttribute("id", submenu.getClientId(), null);
                    if (styleClass != null) {
                        writer.writeAttribute("class", styleClass, null);
                    }
                    if (style != null) {
                        writer.writeAttribute("style", style, null);
                    }
                    writer.writeAttribute("role", "menuitem", null);
                    encodeSubmenu(context, menu, submenu, marginLevel);
                    writer.endElement("li");
                } else if (element instanceof Separator) {
                    encodeSeparator(context, (Separator) element);
                }
            }
        }
    }

    protected void encodeMenuItem(FacesContext context, AbstractMenu menu, MenuItem menuitem, int marginLevel) throws IOException {
        ResponseWriter writer = context.getResponseWriter();
        String title = menuitem.getTitle();

        if (menuitem.shouldRenderChildren()) {
            renderChildren(context, (UIComponent) menuitem);
        } else {
            boolean disabled = menuitem.isDisabled();
            String style = menuitem.getStyle();

            writer.startElement("a", null);
            if (title != null) {
                writer.writeAttribute("title", title, null);
            }

            String styleClass = this.getLinkStyleClass(menuitem);
            if (marginLevel > 0) {
                styleClass = (styleClass == null ? "marginLevel-" + marginLevel : styleClass + " marginLevel-" + marginLevel);
            }
            if (disabled) {
                styleClass = (styleClass == null ? "ui-state-disabled" : styleClass + " ui-state-disabled");
            }

            if (styleClass != null) {
                writer.writeAttribute("class", styleClass, null);
            }

            if (style != null) {
                writer.writeAttribute("style", style, null);
            }

            if (disabled) {
                writer.writeAttribute("href", "#", null);
                writer.writeAttribute("onclick", "return false;", null);
            } else {
                setConfirmationScript(context, menuitem);
                String onclick = menuitem.getOnclick();

                if (marginLevel == 0) {
                    String toggleSubMenu = "Sentinel.toggleSubMenu(this)";
                    onclick = (onclick == null) ? toggleSubMenu : onclick + ";" + toggleSubMenu;
                }

                // GET
                if (menuitem.getUrl() != null || menuitem.getOutcome() != null) {
                    String targetURL = getTargetURL(context, (UIOutcomeTarget) menuitem);
                    writer.writeAttribute("href", targetURL, null);

                    if (menuitem.getTarget() != null) {
                        writer.writeAttribute("target", menuitem.getTarget(), null);
                    }
                } else { // POST
                    writer.writeAttribute("href", "#", null);

                    //UIComponent form = ComponentUtils.findParentForm(context, menu); // PrimeFaces-5.2
                    UIComponent form = ComponentTraversalUtils.closestForm(context, menu); // PrimeFaces-5.3
                    if (form == null) {
                        throw new FacesException("MenuItem must be inside a form element");
                    }

                    String command;
                    if (menuitem.isDynamic()) {
                        String menuClientId = menu.getClientId(context);
                        Map<String, List<String>> params = menuitem.getParams();
                        if (params == null) {
                            params = new LinkedHashMap<String, List<String>>();
                        }
                        List<String> idParams = new ArrayList<String>();
                        idParams.add(menuitem.getId());
                        params.put(menuClientId + "_menuid", idParams);

                        command = menuitem.isAjax() ? buildAjaxRequest(context, menu, (AjaxSource) menuitem, form, params) : buildNonAjaxRequest(context, menu, form, menuClientId, params, true);
                    } else {
                        command = menuitem.isAjax() ? buildAjaxRequest(context, (AjaxSource) menuitem, form) : buildNonAjaxRequest(context, ((UIComponent) menuitem), form, ((UIComponent) menuitem).getClientId(context), true);
                    }

                    onclick = (onclick == null) ? command : onclick + ";" + command;
                }

                if (onclick != null) {
                    if (menuitem.requiresConfirmation()) {
                        writer.writeAttribute("data-pfconfirmcommand", onclick, null);
                        writer.writeAttribute("onclick", menuitem.getConfirmationScript(), "onclick");
                    } else {
                        writer.writeAttribute("onclick", onclick, null);
                    }
                }
            }

            encodeMenuItemContent(context, menu, menuitem);

            writer.endElement("a");
        }
    }

    @Override
    protected String getLinkStyleClass(MenuItem menuItem) {
        String styleClass = menuItem.getStyleClass();
        return styleClass;
    }

    @Override
    protected void encodeMenuItemContent(FacesContext context, AbstractMenu menu, MenuItem menuitem) throws IOException {
        ResponseWriter writer = context.getResponseWriter();
        String icon = menuitem.getIcon();
        Object value = menuitem.getValue();

        if (icon != null) {
            writer.startElement("i", null);
            writer.writeAttribute("class", icon + " yellow i", null);
            writer.endElement("i");
        }

        if (value != null) {
            writer.writeText(value, "value");
        }
    }

    protected void encodeSubmenu(FacesContext context, AbstractMenu menu, Submenu submenu, int marginLevel) throws IOException {
        ResponseWriter writer = context.getResponseWriter();
        String icon = submenu.getIcon();
        String label = submenu.getLabel();
        String style = submenu.getStyle();
        String styleClass = submenu.getStyleClass();

        //title
        writer.startElement("a", null);
        writer.writeAttribute("href", "#", null);
        writer.writeAttribute("onclick", "Sentinel.toggleSubMenu(this);return false;", null);
        if (marginLevel > 0) {
            styleClass = (styleClass == null ? "marginLevel-" + marginLevel : "marginLevel-" + marginLevel + " " + styleClass);
        }
        if (styleClass != null) {
            writer.writeAttribute("class", styleClass, null);
        }
        if (style != null) {
            writer.writeAttribute("style", style, null);
        }

        if (icon != null) {
            writer.startElement("i", null);
            writer.writeAttribute("class", icon + " yellow i", null);
            writer.endElement("i");
        }

        if (label != null) {
            writer.writeText(label, "value");
        }

        encodeSubmenuIcon(context, submenu);

        writer.endElement("a");

        //submenus and menuitems
        if (submenu.getElementsCount() > 0) {
            writer.startElement("ul", null);
            final String TIERED_CHILD_SUBMENU_CLASS = "layout-menubar-submenu-container";
            writer.writeAttribute("class", TIERED_CHILD_SUBMENU_CLASS, null);
            writer.writeAttribute("role", "menu", null);
            encodeElements(context, menu, submenu.getElements(), ++marginLevel);
            writer.endElement("ul");
        }
    }

    protected void encodeSubmenuIcon(FacesContext context, Submenu submenu) throws IOException {
        ResponseWriter writer = context.getResponseWriter();

        writer.startElement("i", null);
        writer.writeAttribute("class", "icon-angle-down Fright", null);
        writer.endElement("i");
    }

    @Override
    protected void encodeSeparator(FacesContext context, Separator separator) throws IOException {
        // no-op
    }
}
